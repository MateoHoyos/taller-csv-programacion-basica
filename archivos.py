import pandas as pd

archivo= pd.read_csv("serviciosurgenciasambulanciasagosto2020.csv", delimiter=";", encoding="ISO-8859-1")
print(archivo)
#1 listado empresas
print(archivo.nombre_prestador)
#2 la cantidad de registros
print(archivo.Id)
#3 las primeras 5 filas
print(archivo.head())

#4 la direccion subred integrada de sevicios de salus sur E.S.E
print(archivo.direccion[13:202])

#5 listado de prestadors d servivio cuyo nombre contenga la palabra "salud"
print(archivo[archivo["nombre_prestador"].str.contains("salud", case= False)])

